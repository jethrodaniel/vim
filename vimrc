unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim

set nowrap
set number

set spell
set spellfile=~/.vim/spell/en.utf-8.add

set modeline
set modelines=1

set splitbelow
set splitright

set wrapscan
set ignorecase

set autoindent
set shiftwidth=2
set tabstop=2
set expandtab

set scrolloff=5
set sidescroll=1

set mouse=

set nobackup
set noundofile
set noswapfile

set laststatus=2
hi StatusLine ctermbg=black ctermfg=green

set path=.,**
" if executable('rg')
" set grepprg='rg --vimgrep --no-heading --smart-case ' shellescape(expand($*))
set grepformat=%f:%l:%c:%m
set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case\ $*
					" Unix: "grep -n $* /dev/null",
" endif

set signcolumn=yes
set updatetime=250

" https://github.com/tpope/vim-commentary/issues/46
set timeoutlen=1000
set ttimeoutlen=100

set showbreak=↪\
set listchars=tab:→\ ,nbsp:&,trail:␣,extends:⟩,precedes:⟨,space:·
" set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+

set wildmenu
set wildmode=longest,list:longest
set wildignore+=*/node_modules/*,*.o

set completeopt+=longest,menuone,noselect,noinsert

" vertical and horizontal cross-hairs on the cursor
" set cursorcolumn

" show horizontal cursor indicator
set cursorline

" show vertical line length indicator
set ruler
set colorcolumn=80

set background=dark

" https://github.com/gruvbox-community/gruvbox/issues/62
set termguicolors
let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_colors = { 'bg0': ['#000000', 0] }
colorscheme gruvbox

hi SpellBad ctermbg=000 cterm=underline

packadd! matchit

if &shell =~# 'fish$'
  set shell=/usr/bin/env bash
endif

match ErrorMsg '\s\+$'

"-- keymaps ----------------------------

" leader-key
let mapleader="\<Space>"
noremap <Space> <Nop>
map <Space> <Leader>

" make j and k go up and down on the screen, ignoring how lines wrap
nnoremap j gj
nnoremap k gk

" move between panes without the ^w prefix
nnoremap <C-h> <C-w><C-h>
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>

" save changes
nnoremap W :w<CR>

" exit
nnoremap X <Esc>:exit!<CR>

" save and exit without the additional Z
nnoremap Z :wq<CR>

" switch horizontal panes to vertical
nnoremap <Leader>v <C-w>t<C-w>H

" switch vertical panes to horizontal
nnoremap <Leader>h <C-w>t<C-w>K

" vimium-style tab movements
nnoremap >> :tabmove +1<CR>
nnoremap << :tabmove -1<CR>

:command! Pwd let @+ = expand('%')

" https://medium.com/@sszreter/vim-tab-autocomplete-in-insert-mode-and-fuzzy-search-for-opening-files-484260f52618
function! InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    else
        return "\<c-n>"
    endif
endfunction
inoremap <expr> <tab> InsertTabWrapper()
inoremap <s-tab> <c-p>

function! Gui()
   if empty($CHROME_GUI)
     let $CHROME_GUI=1
  else
     unlet $CHROME_GUI
  endif
  echo '$CHROME_GUI=' . $CHROME_GUI
endfunction
:command! Gui call Gui()

" less typing
nnoremap ; :


" https://github.com/airblade/vim-gitgutter/issues/614
highlight GitGutterAdd    ctermfg=10 ctermbg=bg
highlight GitGutterChange ctermfg=3 ctermbg=bg
highlight GitGutterDelete ctermfg=9 ctermbg=bg
highlight SignColumn      ctermbg=bg
nnoremap <Leader>n :GitGutterNextHunk<CR>
nnoremap <Leader>b :GitGutterPrevHunk<CR>
nnoremap <Leader>s :GitGutterStageHunk<CR>
nnoremap <Leader>u :GitGutterUndoHunk<CR>
nnoremap <Leader>p :GitGutterPreviewHunk<CR>
let g:gitgutter_preview_win_floating = 1
let g:gitgutter_close_preview_on_escape = 1

nnoremap <silent> <leader>t :TestNearest<CR>
nnoremap <silent> <leader>T :TestFile<CR>
nnoremap <silent> <leader>l :TestLast<CR>

nnoremap <silent> <leader>w :set wrap!<CR>

source ~/.vim/vimrc.d/plugins/ale.vim
source ~/.vim/vimrc.d/plugins/fzf.vim

packloadall
silent! helptags ALL

" never hide anything
set conceallevel=0
