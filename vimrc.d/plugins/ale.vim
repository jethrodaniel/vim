set omnifunc=ale#completion#OmniFunc

let g:ale_completion_enabled=1
let g:ale_fix_on_save=1
let g:ale_lint_delay = 100
let g:ale_lint_on_text_changed='always'
let g:ale_sign_column_always=1

let g:ale_ruby_rubocop_executable='bundle'
let g:ale_rust_rls_toolchain='stable'

let g:ale_fixers = {
\  '*': [
\     'remove_trailing_lines',
\     'trim_whitespace'
\   ],
\  'c': ['clang-format'],
\  'cpp': ['clang-format'],
\  'haml': ['haml-lint'],
\  'javascript': ['eslint'],
\  'ruby': ['rubocop'],
\  'rust': ['rustfmt'],
\}
