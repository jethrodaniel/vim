let g:fzf_preview_window = ['down:60%', 'ctrl-/']
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.9 } }

nnoremap <Leader>f   :Files<CR>
nnoremap <Leader>k   :Rg<Space><C-r><C-w><Cr>
