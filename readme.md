# .vim

> ed is the standard editor

### installation

**NOTE**: these settings that assume you're on vim8.

```
git clone --recurse-submodules https://gitlab.com/jethrodaniel/vim ~/.vim
```

## license

MIT
